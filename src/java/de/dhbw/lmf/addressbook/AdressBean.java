package de.dhbw.lmf.addressbook;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Florian
 */
public class AdressBean {

    private static final String INSERT = "INSERT INTO address (name, christianname, email, addressform, phone, mobile, street, number, city, postcode, country, birthday) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)";
    private static final String UPDATE = "UPDATE address SET name=?, christianname=?, email=?, addressform=?, phone=?, mobile=?, street=?, number=?, city=?, postcode=?, country=?, birthday=? WHERE id=?";
    private static final String DELETE = "DELETE FROM address WHERE id = ?";

    
    private Exception lastError;
    private String id = "";
    private String name = "";
    private String christianname = "";
    private String email = "";
    private String addressform = "";
    private String phone = "";
    private String mobile = "";
    private String street = "";
    private String number = "";
    private String city = "";
    private String postcode = "";
    private String country = "";
    private String birthday = "";

    public AdressBean() {
    }

    public boolean load(String id) {
        Connection con = null;
        try {
            con = DBConfig.getConnection();

            PreparedStatement stmt = con.prepareStatement("SELECT * FROM address WHERE id = ?");
            stmt.setInt(1, Integer.parseInt(id));

            ResultSet rs = stmt.executeQuery();

            if (rs.first()) {
                createAdressFromResultSet(rs, this);

                return true;
            } else {
                throw new SQLException("The given id is invalid!");
            }
        } catch (Exception ex) {
            this.lastError = ex;

            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                //
            }
        }
    }

    public static boolean createAdressFromResultSet(ResultSet rs, AdressBean adr) throws SQLException {
        try {
            adr.setId(rs.getString("id"));
            adr.setName(rs.getString("name"));
            adr.setChristianname(rs.getString("christianname"));
            adr.setEmail(rs.getString("email"));
            adr.setAddressform(rs.getString("addressform"));
            adr.setPhone(rs.getString("phone"));
            adr.setMobile(rs.getString("mobile"));
            adr.setStreet(rs.getString("street"));
            adr.setNumber(rs.getString("number"));
            adr.setCity(rs.getString("city"));
            adr.setPostcode(rs.getString("postcode"));
            adr.setCountry(rs.getString("country"));
            Date d = rs.getDate("birthday");
            DateFormat dF = new SimpleDateFormat("dd.MM.yyyy");
            adr.setBirthday(dF.format(d));
            
            return true;
        } catch (SQLException ex) {
            throw ex;
        }
    }
    
    public boolean delete() {
        if (!isInteger(this.id)) {
            this.lastError = new Exception("There must be an id given in order to delete a contact.");
            return false;
        }
        
        Connection con = null;
        try {
            con = DBConfig.getConnection();

            PreparedStatement stmt = con.prepareStatement(DELETE);
            stmt.setInt(1, Integer.parseInt(this.id));
            
            if (stmt.executeUpdate() > 0) {
                return true;
            }
            
            this.lastError = new Exception("Could not delete the contact - are you sure this id (" + this.id + ") exists?");
            return false;
        } catch (Exception ex) {
            this.lastError = ex;
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException sqlEx) {
                //
            }
        }        
    }
    
    public boolean save() {
        Connection con = null;
        try {
            con = DBConfig.getConnection();

            if (isInteger(this.id)) {
                return this.executeStatment(con.prepareStatement(UPDATE));
            } else {
                return this.executeStatment(con.prepareStatement(INSERT));
            }
        } catch (Exception ex) {
            this.lastError = ex;
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException sqlEx) {
                //
            }
        }
    }

    private boolean executeStatment(PreparedStatement pStat) throws SQLException, ParseException {
        pStat.setString(1, this.name);
        pStat.setString(2, this.christianname);
        pStat.setString(3, this.email);
        pStat.setString(4, this.addressform);
        pStat.setString(5, this.phone);
        pStat.setString(6, this.mobile);
        pStat.setString(7, this.street);
        pStat.setInt(8, Integer.parseInt(this.number));
        pStat.setString(9, this.city);
        pStat.setString(10, this.postcode);
        pStat.setString(11, this.country);
        DateFormat dF = new SimpleDateFormat("dd.MM.yyyy");
        pStat.setDate(12,  new java.sql.Date(dF.parse(this.birthday).getTime()));

        if (!this.id.isEmpty()) {
            pStat.setInt(13, Integer.parseInt(this.id));
        }

        if (pStat.executeUpdate() >= 1) {
            return true;
        }

        return false;
    }

    public ArrayList<String> check() {
        ArrayList<String> errors = new ArrayList<>();
        
        if (this.name.isEmpty()) {
            errors.add("Name is not allowed to be an empty string");
        }

        if (this.christianname.isEmpty()) {
            errors.add("Christianname is not allowed to be an empty string");
        }

        if (this.email.isEmpty()) {
            errors.add("email is not allowed to be an empty string");
        }

        if (!this.email.contains("@")) {
            errors.add("Email seems not to be valid");
        }

        if (this.addressform.isEmpty()) {
            errors.add("Addressform is not allowed to be an empty string");
        }

        if (this.phone.isEmpty()) {
            errors.add("Phone is not allowed to be an empty string");
        }

        if (this.mobile.isEmpty()) {
            errors.add("Mobile is not allowed to be an empty string");
        }

        if (this.street.isEmpty()) {
            errors.add("Street is not allowed to be an empty string");
        }

        if (!isInteger(this.number) || Integer.parseInt(this.number) <= 0) {
            errors.add("Number must be an integer bigger than 0");
        }

        if (this.city.isEmpty()) {
            errors.add("City is not allowed to be an empty string");
        }

        if (this.postcode.isEmpty()) {
            errors.add("Postcode is not allowed to be an empty string");
        }

        if (this.country.isEmpty()) {
            errors.add("Country is not allowed to be an empty string");
        }

        try {
            if (!this.birthday.matches("((0[1-9])|([1-2][0-9])|(3[0-1]))\\.((0[1-9])|(1[0-2])).\\d\\d\\d\\d")) {
                throw new Exception();
            }
            
            DateFormat dF = new SimpleDateFormat("dd.MM.yyyy");
            java.util.Date d = dF.parse(this.birthday);
            if (!d.before(new java.util.Date())) {
                errors.add("Birthday must be before current date");
            }
        } catch (Exception ex) {
            errors.add("Birthday must be in format dd.mm.yyyy (with valid values)");
        }

        return errors;
    }

    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    /**
     * @return the lastError
     */
    public String getLastError() {
        return (lastError == null ? "" : lastError.getMessage());
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = (name == null ? "" : name);
    }

    /**
     * @return the christianname
     */
    public String getChristianname() {
        return christianname;
    }

    /**
     * @param christianname the christianname to set
     */
    public void setChristianname(String christianname) {
        this.christianname = (christianname == null ? "" : christianname);
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = (email == null ? "" : email);
    }

    /**
     * @return the addressform
     */
    public String getAddressform() {
        return addressform;
    }

    /**
     * @param addressform the addressform to set
     */
    public void setAddressform(String addressform) {
        this.addressform = (addressform == null ? "" : addressform);
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = (phone == null ? "" : phone);
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = (mobile == null ? "" : mobile);
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = (street == null ? "" : street);
    }

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(String number) {
        this.number = (number == null ? "" : number);
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = (city == null ? "" : city);
    }

    /**
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @param postcode the postcode to set
     */
    public void setPostcode(String postcode) {
        this.postcode = (postcode == null ? "" : city);
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = (country == null ? "" : country);
    }

    /**
     * @return the birthday
     */
    public String getBirthday() {
        return birthday;
    }
    
    /**
     * @param birthday the birthday to set
     */
    public void setBirthday(String birthday) {
        this.birthday = (birthday == null ? "" : birthday);
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

}
