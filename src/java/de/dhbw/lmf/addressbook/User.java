/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.dhbw.lmf.addressbook;

/**
 *
 * @author Flo
 */
public class User {
    
    private String username;
    private String password;
    private UserType type;

    public User(String username, String password, UserType type) {
        this.username = username;
        this.password = password;
        this.type = type;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the type
     */
    public UserType getType() {
        return type;
    }
    
    public boolean isAdmin() {
        return (this.type == UserType.ADMIN);
    }

    /**
     * @param type the type to set
     */
    public void setType(UserType type) {
        this.type = type;
    }
    
    
    
}
