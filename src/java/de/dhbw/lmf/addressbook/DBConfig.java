package de.dhbw.lmf.addressbook;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Florian
 */
public class DBConfig { 
    private static final String URL = "localhost";
    private static final String DATABASE = "addressbook";
    private static final String USER = "adressbuch";
    private static final String PW = "adressbuch_pw";
    
    public static Connection getConnection() throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection("jdbc:mysql://" + URL + "/" + DATABASE, USER, PW);
    }
}
