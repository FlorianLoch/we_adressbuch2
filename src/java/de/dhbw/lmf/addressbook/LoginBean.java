/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.dhbw.lmf.addressbook;

import java.util.ArrayList;

/**
 *
 * @author Flo
 */
public class LoginBean {
    
    private ArrayList<User> users;
    private int loggedInUser;
    
    public LoginBean() {
        this.users = new ArrayList<User>();
        this.loggedInUser = -1;
        
        this.users.add(new User("admin", "test1234", UserType.ADMIN));
        this.users.add(new User("user", "test1234", UserType.USER));
    }
    
    public boolean isLoggedIn() {
        return (this.loggedInUser != -1);
    }
    
    public boolean isAdmin() {
        return (this.isLoggedIn() && this.users.get(this.loggedInUser).isAdmin());
    }
    
    public boolean login(String username, String passwort) {
        if (username.isEmpty() || passwort.isEmpty()) {
            this.loggedInUser = -1;
            return false;
        }
        
        int i = 0;
        for (User u : this.users) {
            if (u.getUsername().equals(username) && u.getPassword().equals(passwort)) {
                this.loggedInUser = i; 
                return true;
            }
            i++;
        }
        
        this.loggedInUser = -1;
        return false;
    }
    
    public String getUsername() {
        if (this.isLoggedIn()) {
            return this.users.get(this.loggedInUser).getUsername();
        }
        
        return "NOT LOGGED IN";
    }
    
    public void logout() {
        this.loggedInUser = -1;
    }
}
