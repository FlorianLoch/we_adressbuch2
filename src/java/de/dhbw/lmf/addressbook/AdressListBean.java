package de.dhbw.lmf.addressbook;


import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Florian
 */
public class AdressListBean extends ArrayList<AdressBean> {

    private Exception lastError = null;
    private String nameFilter = "";
    
    public AdressListBean() {
    }
    
    public boolean search() {
        Connection con = null;
        try {
            con = DBConfig.getConnection();

            PreparedStatement stmt = null;
            if (this.nameFilter.isEmpty()) {
                stmt = con.prepareStatement("SELECT * FROM address");
            }
            else {
                stmt = con.prepareStatement("SELECT * FROM address WHERE CONCAT_WS(' ', christianname, name) LIKE ?");
                stmt.setString(1, "%" + this.nameFilter + "%");
            }

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                AdressBean adr = new AdressBean();
                AdressBean.createAdressFromResultSet(rs, adr);
                this.add(adr);
            }
            
            return true;
        } catch (Exception ex) {
            this.lastError = ex;

            return false;
        } finally {
            try {
                con.close();
            } catch (Exception ex) {
                //
            }
        }        
    }
    
    /**
     * @return the lastError
     */
    public Exception getLastError() {
        return lastError;
    }

    /**
     * @return the nameFilter
     */
    public String getNameFilter() {
        return nameFilter;
    }

    /**
     * @param nameFilter the nameFilter to set
     */
    public void setNameFilter(String nameFilter) {
        this.nameFilter = (nameFilter == null ? "" : nameFilter);
    }
    
}
