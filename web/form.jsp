<%-- 
    Document   : form
    Created on : 05.05.2014, 16:13:42
    Author     : Florian
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="WEB-INF/jspf/securitycheck.jspf" %>
<c:if test="${!auth.admin}">
    <jsp:forward page="index.jsp"></jsp:forward>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@include file="WEB-INF/jspf/headerIncludes.jspf" %>
    </head>
    <body>
        <div id="wrapper">
            <jsp:useBean id="adr" class="de.dhbw.lmf.addressbook.AdressBean" />         
            <c:if test="${param.id != null}">  
                Edit entry
                <c:set var="loadSuccess" value="${adr.load(param.id)}"/> 
                <c:if test="${!loadSuccess}">
                    <div class="alert alert-danger">
                        Could not load contact Error: ${adr.lastError}
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </div>
                </c:if>
            </c:if>
            <c:if test="${param.id == null}">
                New entry
            </c:if>
            <c:if test="${(param.act != null) && (param.act eq 'save')}">  
                <jsp:setProperty name="adr" property="id" param="id" />
                <jsp:setProperty name="adr" property="addressform" param="addressform" />
                <jsp:setProperty name="adr" property="birthday" param="birthday" />
                <jsp:setProperty name="adr" property="christianname" param="christianname" />
                <jsp:setProperty name="adr" property="city" param="city" />
                <jsp:setProperty name="adr" property="country" param="country" />
                <jsp:setProperty name="adr" property="email" param="email" />
                <jsp:setProperty name="adr" property="mobile" param="mobile" />
                <jsp:setProperty name="adr" property="name" param="name" />
                <jsp:setProperty name="adr" property="number" param="number" />
                <jsp:setProperty name="adr" property="phone" param="phone" />
                <jsp:setProperty name="adr" property="postcode" param="postcode" />
                <jsp:setProperty name="adr" property="street" param="street" />
                <c:set var="checkErrors" value="${adr.check()}"/>              
                <c:if test="${checkErrors.size() > 0}">
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        Could not save contact due to the following error/errors:
                        <ul>
                            <c:forEach items="${checkErrors}" var="err">
                                <li>${err.toString()}</li>
                            </c:forEach>
                        </ul>                  
                    </div>
                </c:if>     
                <c:if test="${checkErrors.size() == 0}">
                    <c:set var="saveSuccess" value="${adr.save()}"/> 
                    <c:if test="${saveSuccess}">
                        <div class="alert alert-success">
                            Successfully saved! 
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        </div>
                    </c:if>                
                    <c:if test="${!saveSuccess}">
                        <div class="alert alert-danger">
                            Error during saving: ${adr.lastError}
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        </div>
                    </c:if>                       
                </c:if>                    
            </c:if>
            <form method="GET">
                <input type="hidden" name="act" value="save"> 
                <c:if test="${param.id != null}">
                    <input type="hidden" name="id" value="${param.id}">
                </c:if>
                <div class="form-group">
                  <div class="col-md-4">
                      <label>Addressform</label>
                      <input type="text" class="form-control" name="addressform" placeholder="Addressform" value="${adr.addressform}">
                  </div>
                  <div class="col-md-4">
                      <label>Christianname</label>
                      <input type="text" class="form-control" name="christianname" placeholder="Christianname" value="${adr.christianname}">
                  </div>    
                  <div class="col-md-4">
                      <label>Name</label>
                      <input type="text" class="form-control" name="name" placeholder="Name" value="${adr.name}">
                  </div>
                </div>
                <div class="form-group col-md-12">
                  <label>Email</label>
                  <input type="text" class="form-control" name="email" placeholder="Email" value="${adr.email}">
                </div>
                <div class="form-group">
                  <div class="col-md-6">
                    <label>Phone</label>
                    <input type="text" class="form-control" name="phone" placeholder="Phone" value="${adr.phone}">
                  </div>
                  <div class="col-md-6">
                    <label>Mobile</label>
                    <input type="text" class="form-control" name="mobile" placeholder="Mobile" value="${adr.mobile}">
                  </div>                    
                </div>  
                <div class="form-group">
                  <div class="col-md-6">
                    <label>Postcode</label>
                    <input type="text" class="form-control" name="postcode" placeholder="Postcode" value="${adr.postcode}">
                  </div>
                  <div class="col-md-6">
                    <label>City</label>
                    <input type="text" class="form-control" name="city" placeholder="City" value="${adr.city}">
                  </div>     
                  <div class="col-md-5">
                    <label>Street</label>
                    <input type="text" class="form-control" name="street" placeholder="Street" value="${adr.street}">
                  </div>
                  <div class="col-md-2">
                    <label>Number</label>
                    <input type="text" class="form-control" name="number" placeholder="Number" value="${adr.number}">
                  </div>      
                  <div class="col-md-5">
                    <label>Country</label>
                    <input type="text" class="form-control" name="country" placeholder="Country" value="${adr.country}">
                  </div>                    
                </div>
                <div class="form-group col-md-12">
                  <label>Birthday</label>
                  <input type="text" class="form-control" name="birthday" placeholder="dd.mm.yyyy" value="${adr.birthday}">
                </div>   
                <button type="submit" class="btn btn-primary">
                    <c:if test="${param.id == null}">Create new</c:if>
                    <c:if test="${param.id != null}">Save changes</c:if>
                </button>
            </form>
            <button class="btn btn-default" onclick="location.href='index.jsp'">Back to list</button>
        </div>
    </body>
</html>
