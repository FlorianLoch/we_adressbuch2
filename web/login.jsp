<%-- 
    Document   : login
    Created on : 25.05.2014, 18:53:30
    Author     : Flo
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<jsp:useBean class="de.dhbw.lmf.addressbook.LoginBean" id="auth" scope="session"></jsp:useBean>
<c:if test="${(param.username != null) && (param.password != null)}">
    <c:set var="loginSuccessful" value="${auth.login(param.username, param.password)}"></c:set>
    <c:if test="${loginSuccessful}">
        <jsp:forward page="index.jsp"></jsp:forward>
    </c:if>
</c:if>
<c:if test="${(!empty param.act) && (param.act eq 'logout') && (auth.loggedIn)}">
    ${auth.logout()}
    Successfully logged out!
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@include file="WEB-INF/jspf/headerIncludes.jspf" %>
    </head>
    <body>
        <div class="col-md-6 col-md-offset-3" style="margin-top: 40px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Login to "Adressbuch2"
                </div>
                <div class="panel-body">
                    <form method="POST">
                        <div class="form-group center-block"> 
                            <input type="text" name="username" class="form-control" placeholder="Username">
                        </div>
                        <div class="form-group center-block"> 
                            <input type="password" name="password" class="form-control" placeholder="Password">
                        </div>
                        <div class="form-group center-block">
                            <input type="submit" class="btn btn-primary center-block">
                        </div>
                    </form>  
                </div>
            </div>
        </div>
    </body>
</html>
