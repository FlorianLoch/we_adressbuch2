<%-- 
    Document   : details
    Created on : 05.05.2014, 16:13:42
    Author     : Florian
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="WEB-INF/jspf/securitycheck.jspf" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@include file="WEB-INF/jspf/headerIncludes.jspf" %>
    </head>
    <body>
        <div id="wrapper">
            <jsp:useBean id="adr" class="de.dhbw.lmf.addressbook.AdressBean" />
            <c:set var="success" value="${adr.load(param.id)}"/>
            <c:if test="${!success}">
                <b>Error:</b> ${adr.lastError}
            </c:if>
            <c:if test="${success}">
                <h2>${adr.addressform} ${adr.christianname} ${adr.name}</h2>
                <br>
                <table class="table table-striped">
                    <tr>
                        <th style="width: 10%;">Email:</th>
                        <td><a href="mailto:${adr.email}">${adr.email}</a></td>
                    </tr>
                    <tr>
                        <th>Phone:</th>
                        <td>${adr.phone}</td>
                    </tr>
                    <tr>
                        <th>Address:</th>
                        <td>
                            ${adr.street} ${adr.number}<br>
                            ${adr.postcode} ${adr.city}<br>
                            ${adr.country}<br>
                            <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#mapModal">
                              Show map
                            </button>                           
                        </td>
                    </tr>
                    <tr>
                        <th>Birthdate:</th>
                        <td>${adr.birthday}</td>
                    </tr>
                </table>
                <!-- Modal -->
                <div class="modal fade" id="mapModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">${adr.addressform} ${adr.christianname} ${adr.name} lives at...</h4>
                      </div>
                      <div class="modal-body" style="height: 500px;">
                          <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.de/maps?hl=en&q=${adr.country} ${adr.postcode} ${adr.street} ${adr.number}&ie=UTF8&t=&z=17&iwloc=B&output=embed"></iframe>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>  
                <!-- End of Modal -->  
                    <c:if test="${auth.admin}">
                    <button class="btn btn-primary" onclick="location.href='form.jsp?id=${adr.id}'">Edit</button>
                </c:if>
            </c:if>            
            <button class="btn btn-default" onclick="location.href='index.jsp'">Back to list</button>
            
        </div>
    </body>
</html>
