<%-- 
    Document   : index
    Created on : 05.05.2014, 09:37:47
    Author     : Florian
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="WEB-INF/jspf/securitycheck.jspf" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@include file="WEB-INF/jspf/headerIncludes.jspf" %>
    </head>
    <body>
        <div id="wrapper">
            <h1>Addressbook</h1>
            <h4>by Maurice Busch, Lukas Schreck and Florian Loch</h4>
            <br>
            <form class="form-inline">
                <div class="form-group">
                    <input type="text" class="form-control" name="q" placeholder="Search for contact...">
                </div>
                <button type="submit" class="btn btn-default">Go</button>
            </form>
            <br>
            <c:if test="${(param.act != null) && (param.id != null) && (param.act eq 'del') && (auth.admin)}">
                <jsp:useBean id="adr" class="de.dhbw.lmf.addressbook.AdressBean" />
                <jsp:setProperty name="adr" property="id" value="${param.id}" />
                <c:set var="deleteSuccess" value="${adr.delete()}" />
                <c:if test="${deleteSuccess}">
                    <div class="alert alert-success">
                        Successfully deleted contact!
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </div>
                </c:if>
                <c:if test="${!deleteSuccess}">
                    <div class="alert alert-danger">
                        An error occured during deletion: ${adr.lastError}
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </div>
                </c:if>
            </c:if>
            
            <jsp:useBean id="list" class="de.dhbw.lmf.addressbook.AdressListBean" />
            <jsp:setProperty name="list" property="nameFilter" param="q" />
            <c:set var="success" value="${list.search()}" />
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Name</th><th>E-Mail</th><th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${list}" var="adr">
                        <tr>
                            <td>${adr.addressform} ${adr.christianname} ${adr.name}</td>
                            <td>${adr.email}</td>
                            <td><a href="details.jsp?id=${adr.id}">Details</a> <a href="index.jsp?act=del&id=${adr.id}">Delete</a></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <c:if test="${auth.admin}">
                <button class="btn btn-primary" onclick="location.href='form.jsp';">Add new contact</button>
            </c:if>
        </div>
    </body>
</html>
